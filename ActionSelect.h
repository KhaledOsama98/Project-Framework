#ifndef ACTIONSELECTA
#define ACTIONSELECTA

#include "Actions\Action.h"
// Select Action Class
class ActionSelect: public Action
{
private:
	ApplicationManager* ApplicationPointer;
public:
	ActionSelect(ApplicationManager *pApp);
	
	//Highlight the selected figure.
	virtual void Execute() ;
	
};
#endif
