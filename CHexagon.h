#pragma once
#include  "Figures\CFigure.h"

#ifndef CHEXAGON_H
#define CHEXAGON_H



class CHexagon : public CFigure
{
private:
	Point TopLeft;
	Point TopRight;
	Point DownLeft;
	Point DownRight;
	Point LeftPoint;
	Point RightPoint;
	
public:
	CHexagon(Point, Point,Point,Point,Point,Point, GfxInfo FigureGfxInfo);
	virtual void DrawMe(GUI* pOut) const;
	bool Contain(int x, int y);
};

#endif