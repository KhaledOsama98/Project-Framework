#include "ActionAddEllipse.h"

#include "CEllipse.h"

#include "ApplicationManager.h"

#include "GUI\GUI.h"

ActionAddEllipse::ActionAddEllipse(ApplicationManager * pApp):Action(pApp)
{}

//Execute the action
void ActionAddEllipse::Execute() 
{
	Point P1,P2;

	//Get a Pointer to the Interface
	GUI* pGUI = pManager->GetGUI();


	GfxInfo ElpsGfxInfo;
	ElpsGfxInfo.isFilled = false;	//default is not filled
	//get drawing, filling colors and pen width from the interface
	ElpsGfxInfo.DrawClr = pGUI->getCrntDrawColor();
	ElpsGfxInfo.FillClr = pGUI->getCrntFillColor();
	ElpsGfxInfo.BorderWdth = pGUI->getCrntPenWidth();


	//Step 1 - Read Square data from the user

	pGUI->PrintMessage("New Ellipse: Click on the top left corner of the rectangle bounding the Ellipse");	
	//Read 1st point and store in point P1
	pGUI->GetPointClicked(P1.x, P1.y);

	pGUI->PrintMessage("Click on the down right corner of the rectangle pounding the Ellipse");
	//Read 2nd point and store in point P2
	pGUI->GetPointClicked(P2.x, P2.y);

	pGUI->ClearStatusBar();


	//Step 2 - prepare Ellipse data
	//User has entered two points P1&P2
	//2.1- Identify the Top left corner of the rectangle bounding the Ellipse
	Point TopLeft ;
	TopLeft.x = P1.x<P2.x? P1.x: P2.x;
	TopLeft.y = P1.y<P2.y? P1.y: P2.y;

	Point DownRight;
	DownRight.x = P1.x<P2.x? P2.x: P1.x;
	DownRight.y = P1.y<P2.y? P2.y: P1.y;

		
	//Step 3 - Create an Ellipse with the parameters read from the user
	CEllipse *R=new CEllipse(TopLeft, DownRight , ElpsGfxInfo);

	//Step 4 - Add the Square to the list of figures
	pManager->AddFigure(R);
}
