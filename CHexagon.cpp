#include "CHexagon.h"


CHexagon::CHexagon(Point P1, Point P2, Point P3, Point P4,Point P5, Point P6, GfxInfo FigureGfxInfo):CFigure(FigureGfxInfo)
{
	TopLeft = P1;
	TopRight = P2;
	DownLeft = P3;
	DownRight = P4;
	LeftPoint = P5;
	RightPoint = P6;
}


void CHexagon::DrawMe(GUI* pGUI) const
{
	//Call Output::DrawEllipe to draw an Ellipse on the screen	
	pGUI->DrawHexagon(TopLeft, TopRight, DownLeft, DownRight, LeftPoint, RightPoint, FigGfxInfo, Selected);
}

bool CHexagon::Contain(int x, int y)
{
	
	double m = 2 / pow(3, 0.5);
	int ymeasured = x*m;
	if (y > TopLeft.y && y<DownLeft.y && x >TopLeft.x && x<DownLeft.x && x>(TopLeft.x - (ymeasured*0.5)) && x < (TopLeft.x + (ymeasured*0.5)))
		return true;
	else
		return false;


}
