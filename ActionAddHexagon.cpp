#include "ActionAddHexagon.h"

#include "CHexagon.h"

#include "ApplicationManager.h"

#include "GUI\GUI.h"

ActionAddHexagon::ActionAddHexagon(ApplicationManager * pApp) :Action(pApp)
{
}

//Execute the action
void ActionAddHexagon::Execute()
{
	Point P1, P2,P3,P4;

	//Get a Pointer to the Interface
	GUI* pGUI = pManager->GetGUI();


	GfxInfo HexGfxInfo;
	HexGfxInfo.isFilled = false;	//default is not filled
									//get drawing, filling colors and pen width from the interface
	HexGfxInfo.DrawClr = pGUI->getCrntDrawColor();
	HexGfxInfo.FillClr = pGUI->getCrntFillColor();
	HexGfxInfo.BorderWdth = pGUI->getCrntPenWidth();


	//Step 1 - Read Square data from the user

	pGUI->PrintMessage("New Hexagon: Click on the top left point of the Hexagon");
	//Read 1st point and store in point P1
	pGUI->GetPointClicked(P1.x, P1.y);

	pGUI->PrintMessage("Click on the down right corner of the Hexagon ");
	//Read 2nd point and store in point P2
	pGUI->GetPointClicked(P2.x, P2.y);

	pGUI->ClearStatusBar();


	//Step 2 - prepare Hexagon data
	//User has entered two points P1&P2
	//2.1- Identify the Top left corner of the rectangle bounding the Ellipse
	Point TopLeft;
	TopLeft.x = P1.x<P2.x ? P1.x : P2.x;
	TopLeft.y = P1.y<P2.y ? P1.y : P2.y;

	Point DownRight;
	DownRight.x = P1.x<P2.x ? P2.x : P1.x;
	DownRight.y = P1.y<P2.y ? P2.y : P1.y;	

	int width = DownRight.x - TopLeft.x;


	Point TopRight;
	TopRight.x = TopLeft.x;
	TopRight.y = DownRight.y;
	
	Point DownLeft;
	DownLeft.x = TopLeft.x;
	DownLeft.y = DownRight.y;
	
	Point RightPoint;
	RightPoint.x = DownRight.x + width/2;
	RightPoint.y = TopLeft.y - DownRight.y/2;

	Point LeftPoint;
	LeftPoint.x = TopLeft.x - width/2;
	RightPoint.y = TopLeft.y - DownRight.y/2;


	//Step 3 - Create an Hexagon with the parameters read from the user
	CHexagon *R = new CHexagon(TopLeft,TopRight,DownLeft, DownRight,LeftPoint,RightPoint, HexGfxInfo);

	//Step 4 - Add the Square to the list of figures
	pManager->AddFigure(R);
}
