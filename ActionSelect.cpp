#include "ActionSelect.h"


#include "ApplicationManager.h"

#include "GUI\GUI.h"

ActionSelect::ActionSelect(ApplicationManager * pApp):Action(pApp)
{
	ApplicationPointer = pApp;
}

//Execute the action
void ActionSelect::Execute() 
{
	Point P1;

//Get a Pointer to the Interface
	GUI* pGUI = pManager->GetGUI();


// Get the points where the user clicked
	pGUI->GetPointClicked(P1.x, P1.y);

	//2.1- Identify the Figure were the user clicked
	CFigure* SelectedFigure= ApplicationPointer->GetFigure(P1.x,P1.y);
	if (SelectedFigure!=NULL)
	{
	//2.2- Identify if this figure is selected or not.
	bool IsSelected =SelectedFigure->IsSelected();
	color DrawColor = SelectedFigure->GetColor();

	//3.1- Select or UnSelect the figure.
	SelectedFigure->SetSelected(!IsSelected);

	//3.2- Highlight the figure if it is Slected.
	if (IsSelected)
		SelectedFigure->ChngDrawClr(UI.HighlightColor);
	else 
		SelectedFigure->ChngDrawClr(GREEN);
	}	
}
