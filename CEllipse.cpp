#include "CEllipse.h"


CEllipse::CEllipse(Point P1, Point P2, GfxInfo FigureGfxInfo):CFigure(FigureGfxInfo)
{
	TopLeftCorner = P1;
	DownRightCorner = P2;
}
	

void CEllipse::DrawMe(GUI* pGUI) const
{
	//Call Output::DrawEllipe to draw an Ellipse on the screen	
	pGUI->DrawEllipse(TopLeftCorner, DownRightCorner, FigGfxInfo, Selected);
}

bool CEllipse::Contain(int x, int y)
{
	int Centerx,Centery,xaxis,yaxis;     // To store the Center of the Ellipse.
	Centerx=TopLeftCorner.x+((DownRightCorner.x-TopLeftCorner.x)/2);
	Centery=TopLeftCorner.y+((DownRightCorner.y-TopLeftCorner.y)/2);
	xaxis=Centerx-TopLeftCorner.x;      //Major axis
	yaxis=Centery-TopLeftCorner.y;      //Minor axis

	if((pow((x-Centerx),2)/pow(xaxis,2))-(pow((y-Centery),2)/pow(yaxis,2))<=1)    //Condition to Check if the point clicked is inside the Ellipse.
		return true;
	else 
		return false;
}