#pragma once
#include "Figures\CFigure.h"


#ifndef CSQUARE_H
#define CSQUARE_H



class CEllipse : public CFigure
{
private:
	Point TopLeftCorner;	
	Point DownRightCorner;
public:
	CEllipse(Point ,Point , GfxInfo FigureGfxInfo );
	virtual void DrawMe(GUI* pOut) const;
	bool Contain(int x, int y);
};

#endif